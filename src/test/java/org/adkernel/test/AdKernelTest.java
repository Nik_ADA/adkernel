package org.adkernel.test;

import org.adkernel.test.map.IntLongMap;
import org.apache.commons.math3.random.RandomDataGenerator;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.HashMap;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class AdKernelTest {
    @Order(1)
    @DisplayName("Test default JAVA HashMap")
    @ParameterizedTest(name = "Add {0} key with random long value")
    @ValueSource(ints = { 10, 100, 1000 })
    void createHashMap(int sizeMap) {
        HashMap<Integer, Long> map = new HashMap<>();
        for (int i = 0; i < sizeMap; i++) {
            int key = new Random().nextInt(i + 1);
            long value = new RandomDataGenerator().nextLong(10L, 1000000000L);
            map.put(key, value);
        }
    }

    @Order(2)
    @DisplayName("Create custom HashMap")
    @ParameterizedTest(name = "Add {0} key with random long value")
    @ValueSource(ints = { 10, 100, 1000 })
    void createMap(int sizeMap) {
        IntLongMap map = new IntLongMap();
        for (int i = 0; i < sizeMap; i++) {
            int key = new Random().nextInt(i + 1);
            long value = new RandomDataGenerator().nextLong(10L, 1000000000L);
            map.put(key, value);
        }
        System.out.println(map);
    }

    @Test
    @Order(4)
    @DisplayName("Get value by key")
    void getValueByKey() {
        int sizeMap = 10;
        IntLongMap map = new IntLongMap();
        for (int i = 0; i < sizeMap; i++) {
            map.put(i, new RandomDataGenerator().nextLong(10L, 1000L));
        }
        int randomKey = new Random().nextInt(map.size());

        System.out.println("GET KEY: " + randomKey + " VALUE:" + map.get(randomKey));
    }

    @Test
    @Order(3)
    @DisplayName("Get size")
    void getSize() {

        int sizeMap = 10;
        IntLongMap map = new IntLongMap();
        for (int i = 0; i < sizeMap; i++) {
            map.put(i, new RandomDataGenerator().nextLong(10L, 1000L));
        }

        assertEquals(sizeMap, map.size());
        System.out.println("SIZE TO CREATE: " + sizeMap + " MAP SIZE:" + map.size());
    }
}