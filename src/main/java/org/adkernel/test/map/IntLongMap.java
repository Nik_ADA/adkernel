package org.adkernel.test.map;

import org.adkernel.test.model.IntLongNode;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Objects;


public class IntLongMap {
    private IntLongNode[] table;
    private int size = 0;
    private float scope;

    /**
     * Initialize empty map with the 8 capacity
     * and the default load factor (0.95)
     */
    public IntLongMap() {
        table = new IntLongNode[8];
        scope = table.length * 0.95f;
    }

    /**
     * Return the value to which the specified key is mapped
     *
     * @param key with which the specified value
     *
     * @return the value to which the specified key is mapped, or -1 if key doesn't exist
     */
    public long get(int key) {
        int hash = hash(key);
        IntLongNode tempTable = table[hash];
        while (Objects.nonNull(tempTable)) {
            if (key == tempTable.getKey()) {
                return tempTable.getValue();
            }
            tempTable = tempTable.getNext();
        }
        return -1;
    }

    /**
     * Add new key-value in this map
     *
     * @param key with which the specified value is to be associated
     * @param value to be associated with the specified key
     */
    public void put(int key, long value) {
        if (size + 1 >= scope) {
            scope *= 2;
            resize();
        }

        int hash = hash(key);
        IntLongNode IntLongNode = table[hash];

        if (Objects.isNull(IntLongNode)) {
            IntLongNode node = new IntLongNode(key, value);
            table[hash] = node;
            size++;
        } else {
            IntLongNode lastNode = null;
            while (Objects.nonNull(IntLongNode)) {
                if (IntLongNode.getKey() == key) {
                    break;
                }

                lastNode = IntLongNode;
                IntLongNode = IntLongNode.getNext();
            }

            if (Objects.nonNull(IntLongNode)) {
                IntLongNode.setValue(value);
            } else {
                lastNode.setNext(new IntLongNode(key, value));
                size++;
            }
        }
    }

    /**
     * Returns the count of entries in this table.
     *
     * @return the count of entries in this table.
     */
    public int size() {
        return size;
    }

    /**
     * Calculates hash code for key
     *
     * @param key to be get hash
     *
     * @return the hash code of the key
     */
    public final int hash(int key) {
        return (table.length - 1) & key; //mo
        //return key % table.length; //if you use this code, you can see open addressing
    }

    /**
     * Set new size for table
     * Increase size in twice from current size
     */
    private void resize() {
        IntLongNode[] oldTable = table;
        table = new IntLongNode[oldTable.length * 2];
        size = 0;
        for (IntLongNode IntLongNode : oldTable) {
            if (Objects.nonNull(IntLongNode)) {
                put(IntLongNode.getKey(), IntLongNode.getValue());
            }
        }
    }

    @Override
    public String toString() {
        ReflectionToStringBuilder reflection = new ReflectionToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE);
        reflection.setExcludeFieldNames("scope");
        reflection.setExcludeNullValues(true);

        return reflection.build();
    }
}
