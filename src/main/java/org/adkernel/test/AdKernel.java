package org.adkernel.test;


import org.adkernel.test.map.IntLongMap;
import org.apache.commons.math3.random.RandomDataGenerator;

import java.util.Random;

public class AdKernel {
    public static void main(String[] args) {
        IntLongMap map = new IntLongMap();

        for (int i = 0; i < 25; i++) {
            int key = new Random().nextInt(i + 1);
            long value = new RandomDataGenerator().nextLong(10L, 1000000000L);
            map.put(key, value);

            System.out.println("Try to add new value: " + i + " > " + key + "=" + value);
        }

        System.out.println("VIEW full map: " + map);
    }
}
