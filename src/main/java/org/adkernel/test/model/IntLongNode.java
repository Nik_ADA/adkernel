package org.adkernel.test.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class IntLongNode {
    private int key;
    private long value;

    private IntLongNode next;


    public IntLongNode(int key, long value) {
        this.key = key;
        this.value = value;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }

    public IntLongNode getNext() {
        return next;
    }

    public void setNext(IntLongNode next) {
        this.next = next;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;

        IntLongNode that = (IntLongNode) obj;

        return new EqualsBuilder()
                .append(key, that.key)
                .append(value, that.value)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(key)
                .append(value)
                .toHashCode();
    }

    @Override
    public String toString() {
        ReflectionToStringBuilder reflections = new ReflectionToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE);
        reflections.setExcludeNullValues(true);

        return reflections.build();
    }
}
